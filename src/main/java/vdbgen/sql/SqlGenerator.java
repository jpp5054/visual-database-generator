/*
    Visual Database Generator - Database Modeling and Generation
    Copyright (C) 2017  Joseph Pomerleau, Joshua Cole,
                        Dominic Sero-Asturi, Nicholas Teslovich
    See /src/main/java/vdbgen/Main.java for full notice.
*/

package vdbgen.sql;

import vdbgen.diagram.Diagram;
import vdbgen.diagram.Entity;

public abstract class SqlGenerator {

    protected StringBuffer sql;
    protected Diagram diagram;

    public SqlGenerator(Diagram diagram) {
        this.diagram = diagram;
    }

    public void setDiagram(Diagram diagram) {
        this.diagram = diagram;
    }

    public abstract String generate();

    abstract void createDatabase(String name);

    abstract void createTable(Entity entity);

}
