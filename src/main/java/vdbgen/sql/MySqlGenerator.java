/*
    Visual Database Generator - Database Modeling and Generation
    Copyright (C) 2017  Joseph Pomerleau, Joshua Cole,
                        Dominic Sero-Asturi, Nicholas Teslovich
    See /src/main/java/vdbgen/Main.java for full notice.
*/

package vdbgen.sql;

import vdbgen.diagram.*;
import vdbgen.diagram.validation.MySqlValidator;

public class MySqlGenerator extends SqlGenerator {

    private static final String DEFAULT_DB_ENGINE = "InnoDB";

    private String databaseEngine = DEFAULT_DB_ENGINE;
    private MySqlValidator validator;

    public MySqlGenerator(Diagram diagram) {
        super(diagram);
    }

    public String generate() {
        sql = new StringBuffer();
        validator = (MySqlValidator) diagram.getValidator();

        if (validator.isValid(diagram, true)) {
            createDatabase(diagram.getName());

            for (Entity entity : diagram.getEntities()) {
                createTable(entity);
            }

            for (Entity entity : diagram.getEntities()) {
                addForeignKeys(entity);
            }
        }

        return sql.toString();
    }

    void createDatabase(String name) {
        assert diagram != null : this;

        sql = new StringBuffer();

        sql.append(String.format("CREATE DATABASE IF NOT EXISTS `%s`;%n", name));
        sql.append(String.format("USE `%s`;%n", name));
    }

    void createTable(Entity entity) {
        assert entity.getName().length() > 0 : entity;

        sql.append(String.format("%nCREATE TABLE IF NOT EXISTS `%s` (%n", entity.getName()));
        addAttributes(entity);
        addPrimaryKey(entity);
        sql.append(String.format(") ENGINE=%s;%n", databaseEngine));
    }

    private String convertType(Attribute.AttributeType type) {
        String result = "";

        switch (type) {
            case INT:
                result = "INT";
                break;
            case BIG_INT:
                result = "BIGINT";
                break;
            case TINY_INT:
                result = "TINYINT";
                break;
            case STRING:
                result = "VARCHAR(300)";
                break;
        }

        return result;
    }

    private void addAttributes(Entity entity) {
        for (int i = 0; i < entity.getAttributes().size(); i++) {
            Attribute a = entity.getAttributes().get(i);

            assert a.getName().length() > 0;

            sql.append( String.format( "  `%s` %s", a.getName(), convertType(a.getType()) ) );

            if (entity.getPrimaryKey().getAttributes().contains(a) && (
                    a.getType() == Attribute.AttributeType.INT ||
                            a.getType() == Attribute.AttributeType.TINY_INT ||
                            a.getType() == Attribute.AttributeType.BIG_INT)) {
                sql.append(" NOT NULL");
            }

            sql.append(String.format(",%n"));
        }
    }

    private void addPrimaryKey(Entity entity) {
        PrimaryKey primaryKey = entity.getPrimaryKey();

        assert primaryKey != null;

        sql.append("  PRIMARY KEY (");

        for (int i = 0; i < primaryKey.getAttributes().size(); i++) {
            sql.append(String.format("`%s`", primaryKey.getAttributes().get(i).getName()));

            if (i < primaryKey.getAttributes().size() - 1) {
                sql.append(", ");
            }
        }

        sql.append(String.format(")%n"));
    }

    private void addForeignKeys(Entity entity) {
        if (entity.getForeignKeys().size() > 0) {
            sql.append(String.format("%nALTER TABLE `%s`%n", entity.getName()));
        }

        for (int i = 0; i < entity.getForeignKeys().size(); i++) {
            ForeignKey foreignKey = entity.getForeignKeys().get(i);
            sql.append("  ADD FOREIGN KEY (");

            for (int j = 0; j < foreignKey.getAttributes().size(); j++) {
                sql.append(String.format("`%s`%s",
                        foreignKey.getAttributes().get(j).getName(),
                        (j < foreignKey.getAttributes().size() - 1 ? ", " : "")));
            }

            sql.append(String.format(") REFERENCES `%s` (", foreignKey.getReferencedEntity().getName()));

            int referencesSize = foreignKey.getReferences().getAttributes().size();

            for (int j = 0; j < referencesSize; j++) {
                Attribute attribute = foreignKey.getReferences().getAttributes().get(j);
                sql.append(String.format("`%s`%s", attribute.getName(), (j < referencesSize - 1 ? ", " : "")));
            }

            sql.append(String.format(")%s%n", (i < entity.getForeignKeys().size() - 1 ? ", " : ";")));
        }
    }
}
