/*
    Visual Database Generator - Database Modeling and Generation
    Copyright (C) 2017  Joseph Pomerleau, Joshua Cole,
                        Dominic Sero-Asturi, Nicholas Teslovich
    See /src/main/java/vdbgen/Main.java for full notice.
*/

package vdbgen.diagram;

import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.shape.Polyline;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Relationship extends Polyline implements DiagramObjectInterface, Serializable {

    public enum RelationshipType {
        ONE_TO_MANY("One To Many"),
        MANY_TO_ONE("Many To One"),
        ONE_TO_ONE("One To One");

        private String name;

        RelationshipType(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public static RelationshipType lookup(String search) throws IllegalArgumentException {
            for (RelationshipType type : RelationshipType.values()) {
                if (search.equals(type.getName())) {
                    return type;
                }
            }

            throw new IllegalArgumentException("Cannot find RelationshipType with name " + search);
        }
    }

    private Entity end1Entity;
    private Entity end2Entity;
    private RelationshipType type;

    private transient Tooltip tooltip;
    private transient Label end1Label;
    private transient Label end2Label;
    private double angle;
    private double label1X;
    private double label1Y;
    private double label2X;
    private double label2Y;

    public Relationship(Entity e1, Entity e2, String typeName) {
        end1Entity = e1;
        end2Entity = e2;

        initialize(typeName);
    }

    private Relationship() {}

    public RelationshipType getType() {
        return type;
    }

    public String getTypeName() {
        return type != null ? type.getName() : "NO RELATIONSHIP TYPE";
    }

    public List<Label> getLabels() {
        List<Label> list = new ArrayList<>();

        if (this.getPoints().size() != 0) {
            list.add(end1Label);
            list.add(end2Label);
        }

        return list;
    }

    public Entity getEnd1Entity() {
        return end1Entity;
    }

    public Entity getEnd2Entity() {
        return end2Entity;
    }

    @Override
    public String getName() {
        return null;
    }

    public void removeFromDiagram() {
        ObservableList<Node> parentNodeList = ((Group) this.getParent()).getChildren();

        parentNodeList.removeAll(this.getLabels());
        parentNodeList.remove(this);

        end1Entity.removeRelationship(this);
        end2Entity.removeRelationship(this);

        ForeignKey foreignKey = null;

        for (ForeignKey key : end2Entity.getForeignKeys()) {
            if (key.getReferences() == end1Entity.getPrimaryKey()) {
                foreignKey = key;
                break;
            }
        }

        if (foreignKey != null) {
            end2Entity.removeForeignKey(foreignKey);
        }

        SelectionManager selectionManager = SelectionManager.getInstance();
        selectionManager.deselect(this);

        int lastIndex = selectionManager.getSelection().size() - 1;

        if (lastIndex >= 0) {
            Node diagramObject = (Node)selectionManager.getSelection().get(lastIndex);
            diagramObject.requestFocus();
        }
    }

    public void changeTooltip(String s) {
        tooltip.setText(s);
    }

    private void initialize(String typeName) {
        tooltip = new Tooltip("Click to Select");
        end1Label = new Label();
        end2Label = new Label();

        Tooltip.install(this, tooltip);

        this.getStyleClass().add("relationship-line");

        if (typeName.equals(RelationshipType.ONE_TO_ONE.getName())) {
            end1Label.setText("1");
            end2Label.setText("1");
            type = RelationshipType.ONE_TO_ONE;
        } else if (typeName.equals(RelationshipType.ONE_TO_MANY.getName())) {
            end1Label.setText("1");
            end2Label.setText("N");
            type = RelationshipType.ONE_TO_MANY;
        } else if (typeName.equals(RelationshipType.MANY_TO_ONE.getName())) {
            end1Label.setText("N");
            end2Label.setText("1");
            type = RelationshipType.MANY_TO_ONE;
        } else {
            end1Label.setText("ERROR 1");
            end2Label.setText("ERROR 2");
        }

        addInputHandlers();
        addListeners();
        repositionConnector();

        end1Entity.addRelationship(this);
        end2Entity.addRelationship(this);
    }

    @Override
    public void addSelectionStyle() {
        this.getStyleClass().add("selected");
        tooltip.setText("Press Delete to remove Relationship");
    }

    @Override
    public void removeSelectionStyle() {
        this.getStyleClass().removeAll("selected");
        tooltip.setText("Click to Select");
    }

    private void addInputHandlers() {
        SelectionManager selectionManager = SelectionManager.getInstance();

        //nodes require focus to listen for key events
        this.setOnMouseClicked(e -> {
            if (selectionManager.isSelected(this)) {
                this.requestFocus();
            } else {
                int selectionSize = selectionManager.getSelection().size();
                if (selectionManager.getSelection().get(selectionSize - 1) instanceof Entity) {
                    ((Entity) selectionManager.getSelection().get(selectionSize - 1)).requestFocus();
                } else {
                    ((Relationship) selectionManager.getSelection().get(selectionSize - 1)).requestFocus();
                }
            }
        });

        this.setOnKeyPressed(e -> {
            final KeyCode key = e.getCode();
            if (key == KeyCode.DELETE) { this.removeFromDiagram(); }
        });

        // Handle Selection
        this.addEventHandler(MouseEvent.MOUSE_PRESSED, e -> {
            this.toFront();

            if (e.isControlDown() || e.isShiftDown()) {

                // Multiple selection
                if (selectionManager.isSelected(this)) {
                    selectionManager.deselect(this);
                } else {
                    selectionManager.select(this);
                    this.requestFocus();
                }

            } else {

                // Single selection
                selectionManager.deselectAll();
                selectionManager.select(this);
                this.requestFocus();
            }
        });
    }

    private void addListeners() {
        end1Entity.boundsInParentProperty().addListener((observable, oldValue, newValue) -> {
            repositionConnector();
        });

        end2Entity.boundsInParentProperty().addListener((observable, oldValue, newValue) -> {
            repositionConnector();
        });
    }

    private void readObject(ObjectInputStream in) {
        try {
            in.defaultReadObject();

            initialize(type.getName());

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void repositionConnector() {
        Bounds e1Bounds;
        Bounds e2Bounds;

        e1Bounds = end1Entity.getBoundsInParent();
        double e1Top = e1Bounds.getMinY();
        double e1Bottom = e1Bounds.getMaxY();
        double e1Right = e1Bounds.getMaxX();
        double e1Left = e1Bounds.getMinX();
        double e1CenterX = e1Left + e1Bounds.getWidth() / 2;
        double e1CenterY = e1Top + e1Bounds.getHeight() / 2;

        e2Bounds = end2Entity.getBoundsInParent();
        double e2Top = e2Bounds.getMinY();
        double e2Bottom = e2Bounds.getMaxY();
        double e2Right = e2Bounds.getMaxX();
        double e2Left = e2Bounds.getMinX();
        double e2CenterX = e2Left + e2Bounds.getWidth() / 2;
        double e2CenterY = e2Top + e2Bounds.getHeight() / 2;

        double e1PointX;
        double e1PointY;
        double e2PointX;
        double e2PointY;

        double length;
        double slope;
        double xOffset;
        double yOffset;
        double slopeBetweenCenters;
        boolean e1Above;

        if (e1CenterY < e2CenterY) {
            //first entity above second
            slopeBetweenCenters = (e2CenterY-e1CenterY)/(e1CenterX-e2CenterX);
            e1Above = true;
        } else {
            //first entity below second
            slopeBetweenCenters = (e1CenterY-e2CenterY)/(e2CenterX-e1CenterX);
            e1Above = false;
        }

        this.getPoints().clear();

        if (slopeBetweenCenters == Double.POSITIVE_INFINITY) {
            // E1 directly above E2
            if (e1Bottom < e2Top && e1Above) {
                //no overlap
                this.getPoints().addAll(e1CenterX, e1Bottom);
                this.getPoints().addAll(e2CenterX, e2Top);
            } else if (e1Top > e2Bottom && !e1Above) {
                //no overlap
                this.getPoints().addAll(e1CenterX, e1Top);
                this.getPoints().addAll(e2CenterX, e2Bottom);
            }
        } else if (slopeBetweenCenters > 0 && e1CenterX < e2CenterX) {
            //E1 below and left of E2
            if (e1Top < e2CenterY) {
                //overlapping Y, not X
                if (e1Right < e2Left) {
                    //E1 left of E2, slightly below (overlapping Y only)
                    //E1 Right Center to E2 Left Center
                    this.getPoints().addAll(e1Right, e1CenterY);
                    this.getPoints().addAll(e2Left, e2CenterY);
                }
            } else {
                if (e1CenterX < e2Left) {
                    //Overlapping X and Y, but can still connect
                    //E1 Top Center to E2 Left Center
                    this.getPoints().addAll(e1CenterX, e1Top);
                    this.getPoints().addAll(e2Left, e2CenterY);
                } else {
                    if (e1Top > e2Bottom) {
                        this.getPoints().addAll(e1CenterX, e1Top);
                        this.getPoints().addAll(e2CenterX, e2Bottom);
                    }
                }
            }
        } else if (slopeBetweenCenters > 0 && e1CenterX > e2CenterX) {
            //E1 above and right of E2
            if (e1Bottom > e2CenterY) {
                //overlapping Y, not X
                if (e1Left > e2Right) {
                    //E1 Left of E2, slightly above (overlapping Y only)
                    //E1 Right Center to E2 Left Center
                    this.getPoints().addAll(e1Left, e1CenterY);
                    this.getPoints().addAll(e2Right, e2CenterY);
                }
            } else {
                if (e1CenterX > e2Right) {
                    this.getPoints().addAll(e1CenterX, e1Bottom);
                    this.getPoints().addAll(e2Right, e2CenterY);
                } else {
                    if (e1Bottom < e2Top) {
                        this.getPoints().addAll(e1CenterX, e1Bottom);
                        this.getPoints().addAll(e2CenterX, e2Top);
                    }
                }
            }
        } else if (slopeBetweenCenters < 0 && e1CenterX < e2CenterX) {
            //E1 above and left of E2
            if (e1Bottom > e2CenterY) {
                //overlapping Y, not X
                if (e1Right < e2Left) {
                    //E1 Left of E2, slightly above (overlapping Y only)
                    //E1 Right Center to E2 Left Center
                    this.getPoints().addAll(e1Right, e1CenterY);
                    this.getPoints().addAll(e2Left, e2CenterY);
                }
            } else {
                if (e1CenterX < e2Left) {
                    this.getPoints().addAll(e1CenterX, e1Bottom);
                    this.getPoints().addAll(e2Left, e2CenterY);
                } else {
                    if (e1Bottom < e2Top) {
                        this.getPoints().addAll(e1CenterX, e1Bottom);
                        this.getPoints().addAll(e2CenterX, e2Top);
                    }
                }
            }
        } else if (slopeBetweenCenters < 0 && e1CenterX > e2CenterX) {
            //E1 below and right of E2
            if (e1Top < e2CenterY) {
                //overlapping Y, not X
                if (e1Left > e2Right) {
                    //E1 left of E2, slightly below (overlapping Y only)
                    //E1 Left Center to E2 Right Center
                    this.getPoints().addAll(e1Left, e1CenterY);
                    this.getPoints().addAll(e2Right, e2CenterY);
                }
            } else {
                if (e1CenterX > e2Right) {
                    //Overlapping X and Y, but can still connect
                    //E1 Top Center to E2 Left Center
                    this.getPoints().addAll(e1CenterX, e1Top);
                    this.getPoints().addAll(e2Right, e2CenterY);
                } else {
                    if (e1Top > e2Bottom) {
                        this.getPoints().addAll(e1CenterX, e1Top);
                        this.getPoints().addAll(e2CenterX, e2Bottom);
                    }
                }
            }
        } else if (slopeBetweenCenters == 0) {
            //E1 on horizontal line with E2
            if (e1CenterX < e2CenterX) {
                //E1 left of E2
                if (e1Right < e2Left) {
                    this.getPoints().addAll(e1Right, e1CenterY);
                    this.getPoints().addAll(e2Left, e2CenterY);
                }
            } else {
                //E1 right of E2
                if (e1Left > e2Right) {
                    this.getPoints().addAll(e1Left, e1CenterY);
                    this.getPoints().addAll(e2Right, e2CenterY);
                }
            }
        }

        if (this.getPoints().size() != 0) {
            e1PointX = this.getPoints().get(0);
            e1PointY = this.getPoints().get(1);
            e2PointX = this.getPoints().get(2);
            e2PointY = this.getPoints().get(3);

            if (e1Above) {
                //first entity above second
                slope = (this.getPoints().get(3) - this.getPoints().get(1)) /
                        (this.getPoints().get(0) - this.getPoints().get(2));
            } else {
                slope = (this.getPoints().get(1) - this.getPoints().get(3)) /
                        (this.getPoints().get(2) - this.getPoints().get(0));
            }

            if (e1Above && slope == Double.POSITIVE_INFINITY) {
                //E1 directly above
                length = e2PointY - e1PointY;
                angle = 90;
                xOffset = 0;
                yOffset = length/4;
                label1X = label2X = e1PointX;
                label1Y = e1PointY + yOffset;
                label2Y = e2PointY - yOffset;
            } else if (!e1Above && slope == Double.POSITIVE_INFINITY) {
                //E1 directly below
                length = e1PointY - e2PointY;
                angle = 90;
                xOffset = 0;
                yOffset = length/4;
                label1X = label2X = e1PointX;
                label1Y = e1PointY - yOffset;
                label2Y = e2PointY + yOffset;
            } else if (e1Above && slope > 0) {
                //E1 above && right
                length = Math.hypot( (e1PointX - e2PointX), (e2PointY - e1PointY));
                angle = Math.toDegrees(Math.atan( (e2PointY - e1PointY) / (e1PointX - e2PointX) ));
                angle = 360 - angle;
                xOffset = Math.cos(Math.toRadians(360 - angle)) * (length / 4);
                yOffset = Math.sin(Math.toRadians(360 - angle)) * (length / 4);
                label1X = e1PointX - xOffset;
                label1Y = e1PointY + yOffset;
                label2X = e2PointX + xOffset;
                label2Y = e2PointY - yOffset;
            } else if (e1Above && slope < 0) {
                //E1 above && left
                length = Math.hypot((e2PointX-e1PointX),(e2PointY-e1PointY));
                angle = Math.toDegrees(Math.atan((e2PointY-e1PointY)/(e2PointX - e1PointX)));
                xOffset = Math.cos(Math.toRadians(angle)) * (length / 4);
                yOffset = Math.sin(Math.toRadians(angle)) * (length / 4);
                label1X = e1PointX + xOffset;
                label1Y = e1PointY + yOffset;
                label2X = e2PointX - xOffset;
                label2Y = e2PointY - yOffset;
            } else if (!e1Above && slope > 0) {
                //E1 below and left
                length = Math.hypot((e2PointX-e1PointX),(e1PointY-e2PointY));
                angle = Math.toDegrees(Math.atan((e1PointY-e2PointY)/(e2PointX - e1PointX)));
                angle = 360 - angle;
                xOffset = Math.cos(Math.toRadians(360 - angle)) * (length / 4);
                yOffset = Math.sin(Math.toRadians(360 - angle)) * (length / 4);
                label1X = e1PointX + xOffset;
                label1Y = e1PointY - yOffset;
                label2X = e2PointX - xOffset;
                label2Y = e2PointY + yOffset;
            } else if (!e1Above && slope < 0) {
                //E1 below && right
                length = Math.hypot((e1PointX-e2PointX),(e1PointY-e2PointY));
                angle = Math.toDegrees(Math.atan((e1PointY-e2PointY)/(e1PointX - e2PointX)));
                xOffset = Math.cos(Math.toRadians(angle)) * (length / 4);
                yOffset = Math.sin(Math.toRadians(angle)) * (length / 4);
                label1X = e1PointX - xOffset;
                label1Y = e1PointY - yOffset;
                label2X = e2PointX + xOffset;
                label2Y = e2PointY + yOffset;
            } else if (slope == 0) {
                if (e1CenterX < e2CenterX) {
                    //E1 directly left
                    length = e2PointX -e1PointX;
                    angle = 0;
                    xOffset = length/4;
                    yOffset = 0;
                    label1X = e1PointX + xOffset;
                    label2X = e2PointX - xOffset;
                    label1Y = label2Y = e1PointY;
                } else {
                    //E1 directly right
                    length = e1PointX - e2PointX;
                    angle = 0;
                    xOffset = length / 4;
                    yOffset = 0;
                    label1X = e1PointX - xOffset;
                    label2X = e2PointX + xOffset;
                    label1Y = label2Y = e1PointY;
                }
            }

            end1Label.setLayoutX(label1X);
            end1Label.setLayoutY(label1Y);
            end2Label.setLayoutX(label2X);
            end2Label.setLayoutY(label2Y);
            end1Label.setRotate(angle);
            end2Label.setRotate(angle);
        }

        this.toBack();
    }
}
