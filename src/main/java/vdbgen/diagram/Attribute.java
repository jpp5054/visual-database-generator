/*
    Visual Database Generator - Database Modeling and Generation
    Copyright (C) 2017  Joseph Pomerleau, Joshua Cole,
                        Dominic Sero-Asturi, Nicholas Teslovich
    See /src/main/java/vdbgen/Main.java for full notice.
*/

package vdbgen.diagram;

import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import vdbgen.diagram.validation.Validator;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

public class Attribute extends HBox implements DiagramObjectInterface, Serializable {

    public enum AttributeType {
        BIG_INT("Big Int"),
        INT("Int"),
        TINY_INT("Tiny Int"),
        STRING("String");

        private String name;

        AttributeType(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public static AttributeType lookup(String search) throws IllegalArgumentException {
            for (AttributeType type : AttributeType.values()) {
                if (search.equals(type.getName())) {
                    return type;
                }
            }

            throw new IllegalArgumentException("Cannot find AttributeType with name " + search);
        }

        @Override
        public String toString() {
            return getName();
        }
    }

    //layout members
    private transient TextField nameField;
    private transient ComboBox<String> attributeType;
    private transient CheckBox pKeyCheckbox;
    private transient Label foreignKeyIdentifier;
    private transient Button deleteButton;

    //data members
    private String name;
    private AttributeType type;
    private Entity parentEntity;
    private boolean hasPrimaryKeyIndicator;
    private boolean hasForeignKeyIndicator;
    private transient Validator validator;

    public Attribute(Entity parent) {
        name = "";
        type = null;
        hasForeignKeyIndicator = false;
        hasPrimaryKeyIndicator = false;

        initialize(parent);
    }

    private void initialize(Entity parent) {
        parentEntity = parent;

        nameField = new TextField(name);
        nameField.setPromptText("Attribute Name");
        nameField.setEditable(false);
        nameField.setCursor(Cursor.HAND);
        nameField.setContextMenu(new ContextMenu());
        HBox.setHgrow(nameField, Priority.ALWAYS);
        this.setAlignment(Pos.CENTER_LEFT);

        attributeType = new ComboBox<>();
        attributeType.setPromptText("Type");

        attributeType.setButtonCell(new ListCell<String>(){
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null){
                    setStyle("-fx-text-fill: #999;");
                } else {
                    setStyle("-fx-text-fill: #000;");
                    setText(item);
                }
            }
        });

        for (AttributeType c : AttributeType.values()) {
            attributeType.getItems().addAll(c.getName());
        }

        if (type != null) {
            attributeType.getSelectionModel().select(type.ordinal());
        }

        pKeyCheckbox = new CheckBox("PK");
        pKeyCheckbox.setTooltip(new Tooltip("Click to toggle primary key"));
        pKeyCheckbox.setSelected(hasPrimaryKeyIndicator);
        foreignKeyIdentifier = new Label(hasForeignKeyIndicator ? "FK" : "");
        foreignKeyIdentifier.setPrefWidth(30);

        deleteButton = new Button("\u00D7");
        deleteButton.getStyleClass().add("delete-btn");

        this.getStyleClass().add("fields-container");
        this.getChildren().addAll(nameField, attributeType, pKeyCheckbox, foreignKeyIdentifier, deleteButton);

        pKeyCheckbox.selectedProperty().addListener((observable, oldValue, newValue) -> {
            hasPrimaryKeyIndicator = newValue;
        });

        addInputHandlers();
    }

    private void readObject(ObjectInputStream in) {
        try {
            in.defaultReadObject();

            initialize(null);

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void setValidator(Validator validator) {
        this.validator = validator;
        validator.addNameChangeListener(nameField, this);
    }

    @Override
    public void addSelectionStyle() {
        this.getStyleClass().add("selected");
    }

    @Override
    public void removeSelectionStyle() {
        this.getStyleClass().removeAll("selected");
    }

    private void addInputHandlers() {
        nameField.setOnKeyPressed(e -> {
            final KeyCode key = e.getCode();
            String oldName = this.name;

            if (key == KeyCode.ENTER || key == KeyCode.ESCAPE) {
                this.requestFocus();

                if (key == KeyCode.ESCAPE) {
                    this.name = oldName;
                    nameField.setText(oldName);
                }
            }
        });

        nameField.focusedProperty().addListener((observable, oldValue, newValue) -> {
            name = nameField.getText();
            SelectionManager.getInstance().deselectAll();
            SelectionManager.getInstance().select(parentEntity);
        });

        // button handler
        deleteButton.setOnAction(e -> {
            boolean isReferenced = parentEntity.primaryKeyIsReferenced();

            if (hasForeignKeyIndicator || (hasPrimaryKeyIndicator && isReferenced)) {
                Alert keyInUseAlert = new Alert(Alert.AlertType.ERROR, null, ButtonType.OK);
                keyInUseAlert.setHeaderText("Cannot remove Attribute \"" + this.name + "\"!");

                if (hasForeignKeyIndicator) {
                    keyInUseAlert.setContentText("It is being used in a Foreign Key.");
                } else {
                    keyInUseAlert.setContentText("It is being used in a Primary Key.");
                }

                keyInUseAlert.showAndWait();
            } else {
                parentEntity.removeAttribute(this);
            }

        });

        // combobox handler
        attributeType.setOnAction(event -> {
            String selected = attributeType.getSelectionModel().getSelectedItem();

            try {
                type = AttributeType.lookup(selected);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        });

        // checkbox handler
        pKeyCheckbox.setOnAction(e -> {
            boolean isSelected = pKeyCheckbox.isSelected();

            if (isSelected) {
                parentEntity.addToPrimaryKey(this);
            } else {
                parentEntity.removeFromPrimaryKey(this);
            }
        });
    }

    public AttributeType getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getTypeName() {
        return (type == null ? "NOT_SET" : type.getName());
    }

    public Entity getParentEntity() {
        return parentEntity;
    }

    @Override
    public String toString() {
        return name;
    }

    public void setParentEntity(Entity parent) {
        parentEntity = parent;
    }

    public void setPrimaryKeyIndicator(boolean setOn) {
        pKeyCheckbox.setSelected(setOn);

        if (setOn) {
            pKeyCheckbox.getStyleClass().add("selected");
        } else {
            pKeyCheckbox.getStyleClass().removeAll("selected");
        }
    }

    public void setPrimaryKeyIndicator() {
        setPrimaryKeyIndicator(true);
    }

    public void setForeignKeyIndicator(boolean setOn) {
        hasForeignKeyIndicator = setOn;
        foreignKeyIdentifier.setText(setOn ? "FK" : "");
    }

    public void setForeignKeyIndicator() {
        setForeignKeyIndicator(true);
    }

    public boolean isForeignKey() {
        return hasForeignKeyIndicator;
    }

    public boolean isPrimaryKey() {
        return hasPrimaryKeyIndicator;
    }
}
