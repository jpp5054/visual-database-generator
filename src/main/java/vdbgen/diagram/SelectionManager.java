/*
    Visual Database Generator - Database Modeling and Generation
    Copyright (C) 2017  Joseph Pomerleau, Joshua Cole,
                        Dominic Sero-Asturi, Nicholas Teslovich
    See /src/main/java/vdbgen/Main.java for full notice.
*/

package vdbgen.diagram;

import java.util.ArrayList;
import java.util.Collections;

public class SelectionManager {

    private static SelectionManager manager;
    private ArrayList<DiagramObjectInterface> selection;

    // Private to enforce Singleton pattern
    private SelectionManager() {
        selection = new ArrayList<>();
    }

    public static SelectionManager getInstance() {
        if (manager == null) {
            manager = new SelectionManager();
        }

        return manager;
    }

    public void select(DiagramObjectInterface object) {
        if (selection.contains(object)) {
            // Move object to top of stack if being "re-selected"
            selection.removeAll(Collections.singleton(object));
        }

        selection.add(object);
        object.addSelectionStyle();
    }

    public void deselect(DiagramObjectInterface object) {
        object.removeSelectionStyle();
        selection.removeAll(Collections.singleton(object));
    }

    public void selectAll(ArrayList<DiagramObjectInterface> objects) {
        for (DiagramObjectInterface o : objects) {
            select(o);
        }
    }

    public void deselectAll() {
        int selectionSize = selection.size();

        for (int i = 0; i < selectionSize; i++) {
            deselect(selection.get(0));
        }
    }

    public boolean isSelected(DiagramObjectInterface object) {
        return selection.contains(object);
    }

    public ArrayList<DiagramObjectInterface> getSelection() {
        return selection;
    }
}
