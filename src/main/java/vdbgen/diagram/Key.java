/*
    Visual Database Generator - Database Modeling and Generation
    Copyright (C) 2017  Joseph Pomerleau, Joshua Cole,
                        Dominic Sero-Asturi, Nicholas Teslovich
    See /src/main/java/vdbgen/Main.java for full notice.
*/

package vdbgen.diagram;

import java.io.Serializable;
import java.util.ArrayList;

public abstract class Key implements Serializable {

    protected ArrayList<Attribute> attributes;
    protected String displayName;

    protected Key() {
        attributes = new ArrayList<>();
    }

    protected Key(ArrayList<Attribute> attributes) {
        this.attributes = attributes;
    }

    protected Key(Attribute attribute) {
        this();
        attributes.add(attribute);
    }

    public ArrayList<Attribute> getAttributes() {
        return attributes;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Entity getParentEntity() {
        Entity parent = null;

        if (this.getAttributes().size() > 0) {
            parent = this.getAttributes().get(0).getParentEntity();
        }

        return parent;
    }
}
