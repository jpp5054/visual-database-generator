/*
    Visual Database Generator - Database Modeling and Generation
    Copyright (C) 2017  Joseph Pomerleau, Joshua Cole,
                        Dominic Sero-Asturi, Nicholas Teslovich
    See /src/main/java/vdbgen/Main.java for full notice.
*/

package vdbgen.diagram;

import java.util.ArrayList;

public class PrimaryKey extends Key {

    public PrimaryKey(ArrayList<Attribute> attributes) {
       super(attributes);

       displayName = "Primary Key";
    }

    public PrimaryKey(Attribute attribute) {
        super(attribute);
    }
}
