/*
    Visual Database Generator - Database Modeling and Generation
    Copyright (C) 2017  Joseph Pomerleau, Joshua Cole,
                        Dominic Sero-Asturi, Nicholas Teslovich
    See /src/main/java/vdbgen/Main.java for full notice.
*/

package vdbgen.diagram;

import java.util.ArrayList;

public class ForeignKey extends Key {

    private PrimaryKey references;

    // Constructor
    public ForeignKey(ArrayList<Attribute> attributes, PrimaryKey references) {
        super(attributes);

        this.references = references;
        displayName = "Foreign Key";
    }

    public ForeignKey(Attribute attribute, PrimaryKey references) {
        super(attribute);
        this.references = references;
    }

    public ForeignKey(Attribute attribute) {
        super(attribute);
    }

    // Cannot construct without attribute(s) and reference PK
    private ForeignKey() {
    }

    public PrimaryKey getReferences() {
        return references;
    }

    public void setReferences(PrimaryKey references) {
        this.references = references;
    }

    public Entity getReferencedEntity() {
        Entity entity = null;

        if (references != null && references.getAttributes().size() > 0) {
            entity = references.getAttributes().get(0).getParentEntity();
        }

        return entity;
    }
}
