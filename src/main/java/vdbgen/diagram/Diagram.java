/*
    Visual Database Generator - Database Modeling and Generation
    Copyright (C) 2017  Joseph Pomerleau, Joshua Cole,
                        Dominic Sero-Asturi, Nicholas Teslovich
    See /src/main/java/vdbgen/Main.java for full notice.
*/

package vdbgen.diagram;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import vdbgen.controllers.ResizeController;
import vdbgen.diagram.validation.MySqlValidator;
import vdbgen.diagram.validation.Validator;
import vdbgen.sql.MySqlGenerator;
import vdbgen.sql.SqlGenerator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Diagram extends Pane implements DiagramObjectInterface, Serializable {

    private transient Group entitiesGroup;
    private transient Group relationshipsGroup;
    private final double DEFAULT_WIDTH = 3000;
    private final double DEFAULT_HEIGHT = 1688;
    private String name;
    private double width;
    private double height;
    private File saveFile;
    private transient SqlGenerator sqlGenerator;
    private transient Validator validator;

    public Diagram() {
        width = DEFAULT_WIDTH;
        height = DEFAULT_HEIGHT;

        initialize();
    }

    public Diagram(double width, double height) {
        this();

        this.width = width;
        this.height = height;
        this.setPrefSize(width, height);
    }

    public Diagram(String name) {
        this();
        this.name = name;
    }

    private void initialize() {
        // NOTE(Joe): In the future, users will select the SQL dialect when creating a new diagram,
        // but for now we default to MySQL.
        sqlGenerator = new MySqlGenerator(this);
        validator = MySqlValidator.build();
        //name = "default_name";

        // Set visual styles
        this.getStyleClass().add("diagram-document");
        this.setPrefSize(width, height);

        // Add groups for z-ordering objects
        entitiesGroup = new Group();
        relationshipsGroup = new Group();
        this.getChildren().add(relationshipsGroup);
        this.getChildren().add(entitiesGroup);

        addInputHandlers();
        this.requestFocus();
    }

    public String getName() {
        return name;
    }

    @Override
    public void addSelectionStyle() {
        // ...
    }

    @Override
    public void removeSelectionStyle() {
        // ...
    }

    public SqlGenerator getSqlGenerator() {
        return sqlGenerator;
    }

    public ArrayList<Entity> getEntities() {
        ArrayList<Entity> entitiesList = new ArrayList<>();

        for (Node node : entitiesGroup.getChildren()) {
            entitiesList.add((Entity) node);
        }

        return entitiesList;
    }

    public ArrayList<Relationship> getRelationships() {
        ArrayList<Relationship> relationshipsList = new ArrayList<>();

        for (Node node : relationshipsGroup.getChildren()) {
            if(!(node instanceof Label)) {
                relationshipsList.add((Relationship) node);
            }
        }

        return relationshipsList;
    }

    public Group getEntitiesGroup() {
        return entitiesGroup;
    }

    public Group getRelationshipsGroup() {
        return relationshipsGroup;
    }

    public ArrayList<DiagramObjectInterface> getAllDiagramObjects() {
        ArrayList<DiagramObjectInterface> objects = new ArrayList<>();

        for (int i = 0; i < entitiesGroup.getChildren().size(); i++) {
            objects.add((DiagramObjectInterface) entitiesGroup.getChildren().get(i));
        }

        for (int i = 0; i < relationshipsGroup.getChildren().size(); i++) {
            if( !(relationshipsGroup.getChildren().get(i) instanceof Label) ) {
                objects.add((DiagramObjectInterface) relationshipsGroup.getChildren().get(i));
            }
        }

        return objects;
    }

    public Validator getValidator() {
        return validator;
    }

    public File getSaveFile() {
        return saveFile;
    }

    public void setSaveFile(File saveFile) {
        this.saveFile = saveFile;
    }

    public void addEntity(Entity entity) {
        entity.setValidator(validator);
        entitiesGroup.getChildren().add(entity);
    }

    public void addRelationship(String type) {
        Entity[] entities = getSelectedEntitiesForRelationship();
        PrimaryKey primaryKey;
        ForeignKey foreignKey;
        Relationship.RelationshipType relationshipType = null;

        try {
            relationshipType = Relationship.RelationshipType.lookup(type);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        if (relationshipType != null && entities != null) {
            primaryKey = entities[0].getPrimaryKey();

            if (primaryKey == null) {
                primaryKey = promptAndCreatePrimaryKey(entities[0]);
            }

            if (primaryKey != null) {
                entities[0].setPrimaryKey(primaryKey);
                foreignKey = promptAndSelectForeignKey(entities, relationshipType);

                if (foreignKey != null) {
                    foreignKey.getParentEntity().addForeignKey(foreignKey);

                    Relationship r = new Relationship(entities[0], entities[1], type);
                    relationshipsGroup.getChildren().add(r);
                    relationshipsGroup.getChildren().addAll(r.getLabels());
                }
            }
        }
    }

    private Entity[] getSelectedEntitiesForRelationship() {
        ArrayList<DiagramObjectInterface> selection = SelectionManager.getInstance().getSelection();
        Entity[] entities = null;
        String errorMessage = "";

        if (selection.size() != 2) {
            errorMessage = "Invalid selection size for relationship creation.";

        } else if (!(selection.get(0) instanceof Entity) || !(selection.get(1) instanceof Entity)) {
            errorMessage = "Only entities may have relationships created between them.";

        } else if (((Entity) selection.get(0)).getAttributes().size() < 1 ||
                ((Entity) selection.get(1)).getAttributes().size() < 1) {

            errorMessage = "No attributes present to make key from.";

        } else {
            entities = new Entity[2];
            entities[0] = (Entity) selection.get(0);
            entities[1] = (Entity) selection.get(1);
        }

        if (errorMessage.length() > 0) {
            Alert selectionAlert = new Alert(Alert.AlertType.ERROR, errorMessage, ButtonType.OK);
            selectionAlert.setHeaderText("Cannot create relationship");
            selectionAlert.showAndWait();
        }

        return entities;
    }

    private PrimaryKey promptAndCreatePrimaryKey(Entity entity) {
        PrimaryKey primaryKey = null;
        List<String> primaryKeyChoices = new ArrayList<>();

        for (Attribute attribute : entity.getAttributes()) {
            primaryKeyChoices.add(attribute.getName());
        }

        ChoiceDialog<String> primaryKeyDialog = new ChoiceDialog<>(null, primaryKeyChoices);
        primaryKeyDialog.setTitle("Create Primary Key: " + entity.getName());
        primaryKeyDialog.setHeaderText("The relationship requires a primary key.");
        primaryKeyDialog.setContentText(
                "Select an attribute from " + entity.getName() + ":");

        Optional<String> primaryKeyChoice = primaryKeyDialog.showAndWait();

        if (!primaryKeyChoice.isPresent()) {

            // TODO: Error handling for no primary key

        } else {
            Attribute primaryKeyAttribute = null;

            for (Attribute attribute : entity.getAttributes()) {
                if (attribute.getName().equals(primaryKeyChoice.get())) {
                    primaryKeyAttribute = attribute;
                    break;
                }
            }

            if (primaryKeyAttribute == null) {

                // TODO: Error handling for attribute not found

            } else {
                primaryKey = new PrimaryKey(primaryKeyAttribute);
            }
        }

        return primaryKey;
    }

    private ForeignKey promptAndSelectForeignKey(Entity[] entities, Relationship.RelationshipType relationshipType) {
        ForeignKey foreignKey = null;
        PrimaryKey primaryKey = entities[1].getPrimaryKey();
        List<String> foreignKeyChoices = new ArrayList<>();
        List<Attribute> attributes = null;

        if (relationshipType == Relationship.RelationshipType.ONE_TO_ONE) {
            if (primaryKey == null || primaryKey.getAttributes().size() < 1) {
                primaryKey = promptAndCreatePrimaryKey(entities[1]);
            }

            if (primaryKey != null) {
                entities[1].setPrimaryKey(primaryKey);
                attributes = new ArrayList<>(entities[0].getPrimaryKey().getAttributes());
                attributes.addAll(entities[1].getPrimaryKey().getAttributes());
            }
        } else {
            attributes = new ArrayList<>(entities[1].getAttributes());
        }

        if (attributes != null) {

            for (Attribute attribute : attributes) {
                foreignKeyChoices.add(attribute.getParentEntity().getName() + ": " + attribute.getName());
            }

            ChoiceDialog<String> foreignKeyDialog = new ChoiceDialog<>(null, foreignKeyChoices);
            foreignKeyDialog.setTitle("Select Foreign Key: " + entities[1].getName());
            foreignKeyDialog.setHeaderText("The relationship requires a foreign key.");

            if (relationshipType == Relationship.RelationshipType.ONE_TO_ONE) {
                foreignKeyDialog.setTitle(
                        "Select Foreign Key: " + entities[0].getName() + " or " + entities[1].getName());
                foreignKeyDialog.setHeaderText(
                        "Must select a primary key attribute to use in \nforeign key for one-to-one relationship.");
                foreignKeyDialog.setContentText(
                        "Select an attribute from the \"secondary\" entity:");
            } else {
                foreignKeyDialog.setContentText(
                        "Select an attribute from " + entities[1].getName() + ":");
            }


            Optional<String> foreignKeyChoice = foreignKeyDialog.showAndWait();

            if (!foreignKeyChoice.isPresent()) {

                // TODO: Error handling for no foreign key selected

            } else {
                Attribute foreignKeyAttribute = null;
                String choice = foreignKeyChoice.get();

                for (Attribute attribute : attributes) {
                    String match = attribute.getParentEntity().getName() + ": " + attribute.getName();

                    if (match.equals(choice)) {
                        foreignKeyAttribute = attribute;
                        break;
                    }
                }

                if (foreignKeyAttribute != null) {
                    PrimaryKey references =
                            (foreignKeyAttribute.getParentEntity() == entities[0])
                                    ? entities[1].getPrimaryKey() : entities[0].getPrimaryKey();
                    foreignKey = new ForeignKey(foreignKeyAttribute, references);
                } else {
                     // TODO: Error handling for attribute not found
                }
            }
        }

        return foreignKey;
    }

    //
    // Handlers
    //

    private void addInputHandlers() {
        this.setOnKeyPressed(e -> {
            final KeyCode key = e.getCode();

            SelectionManager selectionManager = SelectionManager.getInstance();

            if (selectionManager.getSelection().size() == 0) {
                this.requestFocus();
            }

            if (e.isControlDown()) {
               if (key == KeyCode.A) {
                   selectionManager.deselectAll();
                   selectionManager.selectAll(getAllDiagramObjects());

                   int lastIndex = selectionManager.getSelection().size() - 1;

                   if (lastIndex >= 0) {
                       ((Node) selectionManager.getSelection().get(lastIndex)).requestFocus();
                   }
               }
            }
        });

        this.setOnMouseClicked(e -> {
            if (e.getTarget().equals(this)) {
                if (!e.isControlDown() && !e.isShiftDown()) {
                    SelectionManager.getInstance().deselectAll();
                    this.requestFocus();
                }
            }
        });
    }

    //
    // Serialization
    //

    private void writeObject(ObjectOutputStream out) {
        try {
            out.defaultWriteObject();

            ArrayList<Entity> entities = new ArrayList<>();

            for (Node node : entitiesGroup.getChildren()) {
                entities.add((Entity) node);
            }

            ArrayList<Relationship> relationships = new ArrayList<>();

            // Store relationships
            for (Node node : relationshipsGroup.getChildren()) {
                if (node instanceof Relationship) {
                    relationships.add((Relationship) node);
                }
            }

            out.writeObject(entities);
            out.writeObject(relationships);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readObject(ObjectInputStream in) {
        try {
            in.defaultReadObject();

            initialize();

            ArrayList<Entity> entities = (ArrayList<Entity>) in.readObject();
            ArrayList<Relationship> relationships = (ArrayList<Relationship>) in.readObject();

            entitiesGroup.getChildren().addAll(entities);
            relationshipsGroup.getChildren().addAll(relationships);

            for (Entity entity : entities) {
                entity.setValidator(validator);

                for (Attribute attribute : entity.getAttributes()) {
                    attribute.setValidator(validator);
                    attribute.setParentEntity(entity);
                }
            }

            for (int i = 0; i < relationships.size(); i++) {
                this.relationshipsGroup.getChildren().addAll(relationships.get(i).getLabels());
            }


            // Restore resize ability to Entities
            for (Node node : entitiesGroup.getChildren()) {
                ResizeController.makeResizable(node, this);
            }

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
