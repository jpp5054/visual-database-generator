/*
    Visual Database Generator - Database Modeling and Generation
    Copyright (C) 2017  Joseph Pomerleau, Joshua Cole,
                        Dominic Sero-Asturi, Nicholas Teslovich
    See /src/main/java/vdbgen/Main.java for full notice.
*/

package vdbgen.diagram.validation;

import vdbgen.diagram.DiagramObjectInterface;

public class ValidationError {

    public DiagramObjectInterface reference;
    public String message;

    public ValidationError(DiagramObjectInterface reference, String message) {
        int maxLength = 24;
        int length;
        String name;

        this.reference = reference;
        length = reference.getName().length();
        name = length > 0 ? reference.getName().substring(0, Math.min(length, maxLength)) : "";

        this.message = (length > 0 ? "[" + name + (length > maxLength ? "..." : "" ) + "] " : "") + message;
    }

    @Override
    public String toString() {
        return message;
    }
}
