/*
    Visual Database Generator - Database Modeling and Generation
    Copyright (C) 2017  Joseph Pomerleau, Joshua Cole,
                        Dominic Sero-Asturi, Nicholas Teslovich
    See /src/main/java/vdbgen/Main.java for full notice.
*/

package vdbgen.diagram.validation;

import vdbgen.diagram.Attribute;
import vdbgen.diagram.Diagram;
import vdbgen.diagram.DiagramObjectInterface;
import vdbgen.diagram.Entity;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class MySqlValidator extends Validator {

    private static InputStream rulesFile;

    private MySqlValidator(Map<String, Map<String, String>> rules) {
        super(rules);
    }

    public static MySqlValidator build() {
        Map<String, Map<String, String>> rules = new HashMap<>();
        XMLInputFactory inputFactory = XMLInputFactory.newFactory();

        rulesFile = MySqlValidator.class.getResourceAsStream(
                "/vdbgen/support/MySqlValidationRules.xml");

        try {
            XMLStreamReader reader =
                    inputFactory.createXMLStreamReader(rulesFile);

            while (reader.hasNext()) {
                int eventType = reader.next();

                switch (eventType) {
                    case XMLStreamReader.START_ELEMENT:
                        String name = reader.getLocalName();

                        if (name.equals("rules")) {
                            rules = readRules(reader);
                        }

                        break;
                }
            }

            reader.close();

        } catch (XMLStreamException e) {
            e.printStackTrace();
        }

        return new MySqlValidator(rules);
    }

    private static Map<String, Map<String, String>> readRules(XMLStreamReader reader) throws XMLStreamException {
        Map<String, Map<String, String>> rules = new HashMap<>();

        while (reader.hasNext()) {
            int eventType = reader.next();

            switch (eventType) {
                case XMLStreamReader.START_ELEMENT:
                    String name = reader.getLocalName();
                    rules.put(name, readInnerRules(reader));
                    break;
                case XMLStreamReader.END_ELEMENT:
                    return rules;
            }
        }

        throw new XMLStreamException("Did not parse rules successfully");
    }

    private static Map<String, String> readInnerRules(XMLStreamReader reader) throws XMLStreamException {
        Map<String, String> rules = new HashMap<>();

        try {
            while (reader.hasNext()) {
                int eventType = reader.next();

                switch (eventType) {
                    case XMLStreamReader.START_ELEMENT:
                        String name = reader.getLocalName();
                        rules.put(name, reader.getElementText());
                        break;
                    case XMLStreamReader.END_ELEMENT:
                        return rules;
                }
            }
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }

        throw new XMLStreamException("Did not parse rules successfully");
    }

    private Map<String, String> getRulesOfType(String type) {
        Map<String, String> typeRules = null;

        if (rules.containsKey(type)) {
            typeRules = rules.get(type);
        }

        return typeRules;
    }

    public boolean isValid(Attribute attribute, boolean clearErrors) {
        if (clearErrors) {
            errors.clear();
        }

        if (attribute.getType() == null) {
            errors.add(new ValidationError(attribute,
                    "Type Missing: Attribute must have a type selected"));
        }

        return errors.size() == 0;
    }

    public boolean isValid(Entity entity, boolean clearErrors) {
        if (entity.getPrimaryKey() == null || entity.getPrimaryKey().getAttributes().isEmpty()) {
            errors.add(new ValidationError(entity,
                    "Primary Key Missing: Entity must have a primary key"));
        }

        return errors.size() == 0;
    }

    public boolean isValid(Diagram diagram, boolean clearErrors) {
        boolean valid;

        if (clearErrors) {
            errors.clear();
        }

        valid = hasValidName(diagram, false);

        for (Entity e : diagram.getEntities()) {
            valid = isValid(e, false) && valid;
            valid = hasValidName(e, false) && valid;
            valid = !nameIsDuplicated(diagram, e) && valid;

            for (Attribute a : e.getAttributes()) {
                valid = isValid(a, false) && valid;
                valid = hasValidName(a, false) && valid;
                valid = !nameIsDuplicated(e, a) && valid;
            }
        }

        return valid;
    }

    public boolean hasValidName(DiagramObjectInterface diagramObject, boolean clearErrors) {
        String ruleType;
        String name = diagramObject.getName();

        if (diagramObject instanceof Diagram) {
            ruleType = "database";
        } else if (diagramObject instanceof Entity) {
            ruleType = "table";
        } else {
            ruleType = "column";
        }

        Map<String, String> databaseRules = getRulesOfType(ruleType);
        int maxLength = Integer.parseInt(databaseRules.get("maxLength"));
        int minLength = Integer.parseInt(databaseRules.get("minLength"));
        boolean allowSpaceAtEnd = databaseRules.get("allowSpaceAtEnd").equals("true");

        if (clearErrors) {
            errors.clear();
        }

        if (name.length() > maxLength) {
            errors.add(new ValidationError(diagramObject,
                    "Name Invalid: Name for " + ruleType + " must have at most " +
                            maxLength + " characters."));
        } else if (name.length() < minLength) {
            errors.add(new ValidationError(diagramObject,
                    "Name Invalid: Name for " + ruleType + " must have at least " +
                            minLength + " character(s)."));
        }

        if (!allowSpaceAtEnd && name.endsWith(" ")) {
            errors.add(new ValidationError(diagramObject,
                    "Name Invalid: Name for " + ruleType + " cannot end with a space character."));
        }

        return errors.size() == 0;
    }

    public boolean nameIsDuplicated(Diagram parent, Entity entity) {
        boolean isDuplicated = false;

        for (Entity other : parent.getEntities()) {
            if ( entity != other && entity.getName().equals(other.getName()) ) {
                isDuplicated = true;

                errors.add(new ValidationError(entity,
                        "Duplicate: Entity name has duplicate entries in diagram."));

                break;
            }
        }

        return isDuplicated;
    }

    public boolean nameIsDuplicated(Entity parent, Attribute attribute) {
        boolean isDuplicated = false;

        for (Attribute other : parent.getAttributes()) {
            if ( attribute != other && attribute.getName().equals(other.getName()) ) {
                isDuplicated = true;

                errors.add(new ValidationError(attribute,
                        "Duplicate: Attribute name has duplicate entries in entity (" +
                                parent.getName() + ")."));
                break;
            }
        }

        return isDuplicated;
    }
}
