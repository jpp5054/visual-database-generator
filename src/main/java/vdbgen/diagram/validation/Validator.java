/*
    Visual Database Generator - Database Modeling and Generation
    Copyright (C) 2017  Joseph Pomerleau, Joshua Cole,
                        Dominic Sero-Asturi, Nicholas Teslovich
    See /src/main/java/vdbgen/Main.java for full notice.
*/

package vdbgen.diagram.validation;

import javafx.beans.value.ChangeListener;
import javafx.geometry.Side;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import vdbgen.diagram.Attribute;
import vdbgen.diagram.Diagram;
import vdbgen.diagram.DiagramObjectInterface;
import vdbgen.diagram.Entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class Validator {

    protected Map<String, Map<String, String>> rules;
    protected List<ValidationError> errors;

    protected Validator(Map<String, Map<String, String>> rules) {
        errors = new ArrayList<>();
        this.rules = rules;
    }

    abstract public boolean hasValidName(DiagramObjectInterface diagramObject, boolean clearErrors);
    abstract public boolean isValid(Diagram diagram, boolean clearErrors);
    abstract public boolean isValid(Attribute attribute, boolean clearErrors);
    abstract public boolean isValid(Entity entity, boolean clearErrors);

    public List<ValidationError> getErrors() {
        return errors;
    }

    public Map<String, Map<String, String>> getRules() {
        return rules;
    }

    public boolean hasErrors() {
        return !errors.isEmpty();
    }

    public void showErrors(Node node) {
        if (this.hasErrors()) {
            ContextMenu errorsList = new ContextMenu();

            for (ValidationError error : errors) {
                MenuItem errorItem = new MenuItem(error.message);
                errorItem.getStyleClass().add("error-item");
                errorsList.getItems().add(errorItem);
            }

            errorsList.show(node, Side.TOP, 10, -10);
        }
    }

    public void addNameChangeListener(TextField nameField, DiagramObjectInterface diagramObject) {

        ChangeListener<Boolean> nameChangeListener = (observable, oldValue, newValue) -> {
            if (newValue) {
                nameField.setEditable(true);
                nameField.getStyleClass().add("editing");
                nameField.setCursor(Cursor.DEFAULT);
            } else {
                nameField.getStyleClass().removeAll("editing");
                nameField.setCursor(Cursor.HAND);

                if (!this.hasValidName(diagramObject, true)) {
                    nameField.getStyleClass().add("error");
                    this.showErrors(nameField);
                } else {
                    nameField.getStyleClass().removeAll("error");
                }
            }
        };

        nameField.focusedProperty().addListener(nameChangeListener);
    }
}
