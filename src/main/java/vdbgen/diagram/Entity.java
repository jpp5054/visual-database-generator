/*
    Visual Database Generator - Database Modeling and Generation
    Copyright (C) 2017  Joseph Pomerleau, Joshua Cole,
                        Dominic Sero-Asturi, Nicholas Teslovich
    See /src/main/java/vdbgen/Main.java for full notice.
*/

package vdbgen.diagram;

import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyDoubleWrapper;
import javafx.geometry.Bounds;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import vdbgen.diagram.validation.Validator;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class Entity extends VBox implements DiagramObjectInterface, Serializable {

    private static final double DEFAULT_WIDTH = 280.0;

    // Layout members
    private transient VBox attributesContainer;
    private transient GridPane header;
    private transient TextField nameField;
    private transient Button collapseButton;
    private transient Button addAttributeButton;
    private transient Button delEntityBtn;

    private transient ReadOnlyDoubleWrapper centerX = new ReadOnlyDoubleWrapper();
    private transient ReadOnlyDoubleWrapper centerY = new ReadOnlyDoubleWrapper();

    // Data members
    private String name;
    private double xPos;
    private double yPos;
    private double width;
    private double height;
    private ArrayList<Attribute> attributes;
    private transient ArrayList<Relationship> relationships;
    private ArrayList<ForeignKey> foreignKeys;
    private PrimaryKey primaryKey;
    private transient Validator validator;


    public Entity() {
        this("DefaultName");
    }

    public Entity(double x, double y) {
        this("New Table", x, y);
    }

    public Entity(String name, double x, double y) {
        this(name);
        this.setLayoutX(x);
        this.setLayoutY(y);
    }

    public Entity(String name) {
        this.name = name;

        initialize();

        this.setLayoutX(0.0);
        this.setLayoutY(0.0);
        this.setPrefWidth(DEFAULT_WIDTH);
        this.setAlignment(Pos.CENTER_LEFT);

        addInputHandlers();

        this.boundsInParentProperty().addListener((observable, oldValue, newValue) -> {
            updatePosition(newValue);
        });
    }

    @Override
    public String toString() {
        return "Entity[name=" + name + ", attributes=" + attributes.toString() + ", relationships="
                + relationships.toString() + "]";
    }

    private void initialize() {
        relationships = new ArrayList<>();
        attributes = (attributes == null) ? new ArrayList<>() : attributes;
        foreignKeys = (foreignKeys == null) ? new ArrayList<>(): foreignKeys;
        validator = null;

        addAttributeButton = new Button("+");
        delEntityBtn = new Button("\u00D7");

        nameField = new TextField(name);
        nameField.setEditable(false);
        nameField.setCursor(Cursor.HAND);
        nameField.setContextMenu(new ContextMenu());

        header = new GridPane();
        header.add(nameField, 0, 0);
        header.add(addAttributeButton, 1, 0);
        header.add(delEntityBtn, 2, 0);
        header.setAlignment(Pos.CENTER_LEFT);
        GridPane.setHgrow(nameField, Priority.ALWAYS);

        attributesContainer = new VBox();
        this.getChildren().addAll(header, attributesContainer);
        VBox.setVgrow(attributesContainer, Priority.ALWAYS);

        // Set style classes
        this.getStyleClass().add("entity-container");
        header.getStyleClass().add("entity-header");
        attributesContainer.getStyleClass().addAll("attributes-container");
        delEntityBtn.getStyleClass().add("delete-btn");
        addAttributeButton.getStyleClass().add("add-btn");

        this.setFillWidth(true);
        this.setMinHeight(header.getHeight() + 75.0);
        this.setAlignment(Pos.CENTER_LEFT);

        centerX = new ReadOnlyDoubleWrapper();
        centerY = new ReadOnlyDoubleWrapper();
    }


    private void writeObject(ObjectOutputStream out) {
        try {
            width = this.getPrefWidth();
            height = this.getPrefHeight();

            out.defaultWriteObject();
        } catch (IOException e) {
           e.printStackTrace();
        }
    }

    private void readObject(ObjectInputStream in) {
        try {
            in.defaultReadObject();

            this.setLayoutX(xPos);
            this.setLayoutY(yPos);
            this.setPrefSize(width, height);

            initialize();

            attributesContainer.getChildren().addAll(attributes);

            for (Attribute attribute : attributes) {
                attribute.setParentEntity(this);
            }

            addInputHandlers();

            this.boundsInParentProperty().addListener((observable, oldValue, newValue) -> {
                updatePosition(newValue);
            });

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    //
    // Position updates
    //

    private void updatePosition(Bounds bounds) {
        centerX.set(bounds.getMinX() + bounds.getWidth() / 2);
        centerY.set(bounds.getMinY() + bounds.getHeight() / 2);
        xPos = bounds.getMinX();
        yPos = bounds.getMinY();
    }

    public ReadOnlyDoubleProperty centerXProperty() {
        return centerX.getReadOnlyProperty();
    }

    public ReadOnlyDoubleProperty centerYProperty() {
        return centerY.getReadOnlyProperty();
    }


    //
    // Accessors
    //

    public double getXPos() {
        return xPos;
    }

    public double getYPos() {
        return yPos;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Relationship> getRelationships() {
        return relationships;
    }

    public ArrayList<Attribute> getAttributes() {
        return attributes;
    }

    public ArrayList<ForeignKey> getForeignKeys() {
        return this.foreignKeys;
    }

    public PrimaryKey getPrimaryKey() {
        return primaryKey;
    }

    //
    // Mutators
    //

    public void setPrimaryKey(PrimaryKey key) {
        primaryKey = key;

        if (primaryKey == null) {
            deletePrimaryKey();
        } else {
            for (Attribute attribute : primaryKey.getAttributes()) {
                attribute.setPrimaryKeyIndicator();
            }
        }
    }

    public void addToPrimaryKey(Attribute attribute) {
        if (primaryKey != null && isInRelationship(Relationship.RelationshipType.ONE_TO_ONE)) {
            attribute.setPrimaryKeyIndicator(false);
            showPrimaryKeyErrorDialog();
        } else {
            if (primaryKey == null) {
                primaryKey = new PrimaryKey(attribute);
            } else if (!primaryKey.getAttributes().contains(attribute)) {
                primaryKey.getAttributes().add(attribute);
            }

            attribute.setPrimaryKeyIndicator();
        }
    }

    public void removeFromPrimaryKey(Attribute attribute) {
        if (primaryKey != null && attributeIsInPrimaryKey(attribute)) {
            if (primaryKeyIsReferenced()) {
                attribute.setPrimaryKeyIndicator(true);
                showPrimaryKeyErrorDialog();
            } else {
                primaryKey.getAttributes().removeAll(Collections.singleton(attribute));
                attribute.setPrimaryKeyIndicator(false);
            }

            if (primaryKey.getAttributes().size() == 0) {
                primaryKey = null;
            }
        }
    }

    public void deletePrimaryKey() {
        if (primaryKey != null) {
            for (Attribute attribute : primaryKey.getAttributes()) {
                attribute.setPrimaryKeyIndicator(false);
            }

            primaryKey = null;
        }
    }

    public void addForeignKey(ForeignKey key) {
        if (key != null) {
            foreignKeys.add(key);

            for (Attribute attribute : key.getAttributes()) {
                attribute.setForeignKeyIndicator();
            }
        }
    }

    public void removeForeignKey(ForeignKey key) {
        if (foreignKeys != null) {
            foreignKeys.remove(key);

            for (Attribute attribute : key.getAttributes()) {
                attribute.setForeignKeyIndicator(false);
            }
        }
    }

    private void showPrimaryKeyErrorDialog() {
        Alert primaryKeyAlert = new Alert(Alert.AlertType.ERROR, null, ButtonType.OK);
        primaryKeyAlert.setHeaderText("Cannot change Primary Key!");
        primaryKeyAlert.setContentText("It is being used in a relationship.");
        primaryKeyAlert.showAndWait();
    }

    public void setForeignKeys(ArrayList<ForeignKey> foreignKeys) {
        this.foreignKeys = foreignKeys;
    }

    public boolean primaryKeyIsReferenced() {
        boolean isReferenced = false;

        for (Relationship relationship : relationships) {
            Entity other;

            if (relationship.getType() == Relationship.RelationshipType.ONE_TO_ONE) {
                isReferenced = true;
                break;
            }

            if (relationship.getEnd1Entity() != this) {
                other = relationship.getEnd1Entity();
            } else {
                other = relationship.getEnd2Entity();
            }

            for (ForeignKey key : other.getForeignKeys()) {
                if (key.getReferences() == primaryKey) {
                    isReferenced = true;
                    break;
                }
            }
        }

        return isReferenced;
    }

    private boolean isInRelationship(Relationship.RelationshipType type) {
        boolean result = false;

        for (Relationship relationship : relationships) {
            if (relationship.getType() == type) {
                result = true;
                break;
            }
        }

        return result;
    }

    private boolean attributeIsInPrimaryKey(Attribute attribute) {
        boolean isInPrimaryKey = false;

        if (primaryKey != null) {
            for (Attribute a : primaryKey.getAttributes()) {
                if (a == attribute) {
                    isInPrimaryKey = true;
                    break;
                }
            }
        }

        return isInPrimaryKey;
    }

    public void addAttribute(Attribute attr) {
        attr.setValidator(validator);
        attributes.add(attr);
        attributesContainer.getChildren().add(attr);
        this.setPrefHeight(USE_COMPUTED_SIZE);
        this.setMinHeight(USE_COMPUTED_SIZE);
    }

    public void removeAttribute(Attribute attr) {
        removeFromPrimaryKey(attr);
        attributes.remove(attr);
        attributesContainer.getChildren().remove(attr);
    }

    public void addRelationship(Relationship relationship) {
        if (!relationships.contains(relationship)) {
            relationships.add(relationship);
        }
    }

    public void removeRelationship(Relationship relationship) {
        relationships.remove(relationship);
    }

    public void removeFromDiagram() {
        ((Group) this.getParent()).getChildren().remove(this);

        int size = relationships.size();

        for (int i = 0; i < size; i++) {
            relationships.get(0).removeFromDiagram();
        }

        SelectionManager selectionManager = SelectionManager.getInstance();
        selectionManager.deselect(this);
        if (selectionManager.getSelection().size() > 0) {
            int i = selectionManager.getSelection().size() - 1;
            if (selectionManager.getSelection().get(i) instanceof Entity) {
                ((Entity) selectionManager.getSelection().get(i)).requestFocus();
            } else if (selectionManager.getSelection().get(i) instanceof Relationship) {
                ((Relationship) selectionManager.getSelection().get(i)).requestFocus();
            }
        }
    }

    public void setValidator(Validator validator) {
        this.validator = validator;
        validator.addNameChangeListener(nameField, this);
    }

    @Override
    public void addSelectionStyle() {
        this.getStyleClass().add("selected");
    }

    @Override
    public void removeSelectionStyle() {
        this.getStyleClass().removeAll("selected");
    }

    private void addInputHandlers() {
        SelectionManager selectionManager = SelectionManager.getInstance();

        nameField.focusedProperty().addListener((observable, oldValue, newValue) -> {
            name = nameField.getText();
            selectionManager.deselectAll();
            selectionManager.select(this);
        });

        //nodes require focus to listen for key events
        this.setOnMouseClicked(e -> {
            if(selectionManager.isSelected(this)) {
                this.requestFocus();
            } else {
                int lastIndex = selectionManager.getSelection().size() - 1;

                if (lastIndex >= 0) {
                    if (selectionManager.getSelection().get(lastIndex) instanceof Entity) {
                        ((Entity) selectionManager.getSelection().get(lastIndex)).requestFocus();
                    } else {
                        ((Relationship) selectionManager.getSelection().get(lastIndex)).requestFocus();
                    }
                }
            }
        });

        this.setOnKeyPressed(e -> {
            final KeyCode key = e.getCode();
            if (key == KeyCode.DELETE) {
                this.removeFromDiagram();
            }
        });

        nameField.setOnKeyPressed(e -> {
            final KeyCode key = e.getCode();
            String oldName = this.name;

            if (key == KeyCode.ENTER || key == KeyCode.ESCAPE) {
                this.requestFocus();

                if (key == KeyCode.ESCAPE) {
                    this.name = oldName;
                    nameField.setText(oldName);
                }
            }
        });

        addAttributeButton.setOnAction(e -> {
            SelectionManager.getInstance().deselectAll();
            SelectionManager.getInstance().select(this);
            this.addAttribute(new Attribute(this));
        });

        // Handle Selection
        this.addEventHandler(MouseEvent.MOUSE_PRESSED, e -> {
            this.toFront();

            if (e.isControlDown() || e.isShiftDown()) {

                // Multiple selection
                if (selectionManager.isSelected(this)) {
                    selectionManager.deselect(this);
                } else {
                    selectionManager.select(this);
                }
            } else {

                // Single selection
                selectionManager.deselectAll();
                selectionManager.select(this);
            }
        });

        delEntityBtn.setOnAction(e -> {
            this.removeFromDiagram();
        });
    }
}
