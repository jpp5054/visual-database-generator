/*
    Visual Database Generator - Database Modeling and Generation
    Copyright (C) 2017  Joseph Pomerleau, Joshua Cole,
                        Dominic Sero-Asturi, Nicholas Teslovich
    See /src/main/java/vdbgen/Main.java for full notice.
*/

package vdbgen.controllers;

import com.sun.javafx.stage.StageHelper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.*;
import javafx.scene.image.WritableImage;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import vdbgen.diagram.Diagram;
import vdbgen.diagram.Entity;
import vdbgen.diagram.Relationship;
import vdbgen.diagram.SelectionManager;
import vdbgen.diagram.validation.ValidationError;
import vdbgen.documentation.DocumentationGenerator;

import javax.imageio.ImageIO;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.ResourceBundle;


public class ApplicationController implements Initializable {

    // Layout
    @FXML private VBox root;
    @FXML private ScrollPane diagramScrollPane;
    @FXML private StackPane diagramArea;
    @FXML private HBox toolbar;
    @FXML private HBox statusBar;

    // Buttons
    @FXML private Button addEntityBtn;
    @FXML private Button addRelationshipBtn;
    @FXML private ComboBox<String> relationshipTypeBtn;
    @FXML private Button confirmBtn;
    @FXML private Button cancelBtn;

    // Menus
    @FXML private Menu fileMenu;
    @FXML private Menu editMenu;
    @FXML private Menu toolsMenu;
    @FXML private Menu helpMenu;

    // Data
    private Diagram activeDiagram = null;
    private Stage primaryStage;
    private File recentSaveLoadDirectory;

    private static ApplicationController controller;

    private double x = 10;
    private double y = 10;
    private double xOffset = 40;
    private double yOffset = 20;
    private boolean cancel = false;
    private boolean confirm = false;
    private String aboutText;

    public void initialize(URL url, ResourceBundle resourceBundle) {
        root.getStylesheets().add("/vdbgen/styles/application.css");
        root.getStylesheets().add("/vdbgen/styles/diagram.css");

        aboutText = loadAboutTextFromFile();

        for (Relationship.RelationshipType r : Relationship.RelationshipType.values()) {
            relationshipTypeBtn.getItems().addAll(r.getName());
        }

        relationshipTypeBtn.setPromptText("Relationship Type");
        relationshipTypeBtn.setEditable(false);

        diagramScrollPane.setContent(diagramArea);

        controller = this;

        for (Node item : toolbar.getChildren()) {
            item.setDisable(true);
        }

        relationshipTypeBtn.setButtonCell(new ListCell<String>(){
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null){
                    // styled like -fx-prompt-text-fill:
                    setStyle("-fx-text-fill: white; -fx-font-weight: bold;");
                    confirmBtn.setDisable(true);
                } else {
                    setStyle("-fx-text-fill: #ffff72; -fx-font-weight: bold;");
                    setText(item);
                    confirmBtn.setDisable(false);
                    cancelBtn.setDisable(false);
                }
            }
        });


        EventHandler<ActionEvent> fileMenuHandler = e -> {
            String name = ((MenuItem) e.getTarget()).getText();

            switch (name) {
                case "New": newDiagram();
                    break;
                case "Open": openDiagram();
                    break;
                case "Save": saveActiveDiagram();
                    break;
                case "Close":
                    promptToSave();
                    closeActiveDiagram();
                    break;
                case "Exit":
                    primaryStage.fireEvent(new WindowEvent(primaryStage, WindowEvent.WINDOW_CLOSE_REQUEST));
                    break;
            }
        };

        EventHandler<ActionEvent> editMenuHandler = e -> {
            String name = ((MenuItem) e.getTarget()).getText();

            if (activeDiagram != null) {
                switch (name) {
                    case "Select All":
                        SelectionManager.getInstance().selectAll(activeDiagram.getAllDiagramObjects());
                        break;
                    case "Deselect All":
                        SelectionManager.getInstance().deselectAll();
                        break;
                }
            }

        };

        EventHandler<ActionEvent> toolsMenuHandler = e -> {
            String name = ((MenuItem) e.getTarget()).getText();

            if (activeDiagram != null) {
                switch (name) {
                    case "Generate SQL": showGeneratedSql();
                        break;
                    case "Generate Docs": DocumentationGenerator.getInstance().generatePDF();
                        break;
                    case "Export Image": exportDiagramImage(activeDiagram);
                        break;
                }
            }
        };

        EventHandler<ActionEvent> helpMenuHandler = e -> {
            String name = ((MenuItem) e.getTarget()).getText();

            if (name.equals("About")) {
                showAboutWindow();
            }
        };

        fileMenu.setOnAction(fileMenuHandler);
        editMenu.setOnAction(editMenuHandler);
        toolsMenu.setOnAction(toolsMenuHandler);
        helpMenu.setOnAction(helpMenuHandler);
    }

    public Diagram getActiveDiagram() {
        return activeDiagram;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public void setPrimaryStage(Stage stage) {
        primaryStage = stage;
        primaryStage.setOnCloseRequest(e -> promptToSave());
    }

    public static ApplicationController getInstance() {
        if (controller == null) {
            controller = new ApplicationController();
        }

        return controller;
    }

    @FXML
    public void addEntity() {
        Entity entity = new Entity(x, y);
        ResizeController.makeResizable(entity, activeDiagram);
        activeDiagram.addEntity(entity);
        x += 60;
        y += 30;

        //TODO: find what determines entity start dimensions and add them to X and Y for this comparison
        if(activeDiagram.getPrefWidth() < x || activeDiagram.getPrefHeight() < y){
            x = xOffset;
            y = yOffset;
            xOffset += 20;
            yOffset += 20;
            if(xOffset > 500 || yOffset > 500){
                xOffset = 40;
                yOffset = 20;
            }
        }

        activeDiagram.requestFocus();
    }

    @FXML
    public void addRelationship() {
        SelectionManager selectionManager = SelectionManager.getInstance();
        String errorMessage = "";

        relationshipTypeBtn.setDisable(false);
        cancelBtn.setDisable(false);
        confirm = false;
        cancel = false;

        for (int i = 0; !cancel && i < selectionManager.getSelection().size(); i++) {
            if (selectionManager.getSelection().get(i) instanceof Relationship) {
                cancel = true;
                errorMessage = "Relationship cannot be built with relationship selected.";
            }
        }

        if (!cancel && selectionManager.getSelection().size() != 2) {
            errorMessage = "Incorrect number of entities selected for relationship building.";
            cancel = true;
        }

        if (cancel) {
            cancelRelationship();
        }

        if (errorMessage.length() > 0) {
            Alert selectionAlert = new Alert(Alert.AlertType.ERROR, errorMessage, ButtonType.OK);
            selectionAlert.setHeaderText("Cannot create relationship");
            selectionAlert.showAndWait();
        }
    }

    @FXML
    public void confirmRelationship() {
        confirm = true;
        activeDiagram.addRelationship(relationshipTypeBtn.getSelectionModel().getSelectedItem());
        resetRelationshipState();
    }

    @FXML
    public void cancelRelationship() {
        cancel = true;
        resetRelationshipState();
    }

    private void resetRelationshipState() {
        relationshipTypeBtn.getSelectionModel().clearSelection();
        relationshipTypeBtn.setDisable(true);
        confirmBtn.setDisable(true);
        cancelBtn.setDisable(true);
        SelectionManager.getInstance().deselectAll();
        activeDiagram.requestFocus();
    }

    private void closeDialogWindows() {
        for (int i = 0; i < StageHelper.getStages().size(); i++) {
            Stage stage = StageHelper.getStages().get(i);

            if (stage != primaryStage) {
                stage.close();
            }
        }
    }

    private void showGeneratedSql() {
        activeDiagram.requestFocus();
        String generatedSql = activeDiagram.getSqlGenerator().generate();

        closeDialogWindows();

        if (!generatedSql.isEmpty()) {
            showGeneratedSqlDialog(generatedSql);
        } else {
            showValidationErrorsDialog();
        }
    }

    private void showGeneratedSqlDialog(String generatedSql) {
        Alert sqlDialog = new Alert(Alert.AlertType.NONE);
        ButtonType copyButtonType = new ButtonType("Copy to Clipboard", ButtonBar.ButtonData.LEFT);
        sqlDialog.getButtonTypes().addAll(ButtonType.CLOSE, copyButtonType);

        TextArea sqlTextArea = new TextArea(generatedSql);
        sqlTextArea.setFont(Font.font("Consolas"));
        sqlTextArea.setEditable(false);
        sqlTextArea.setPadding(Insets.EMPTY);

        sqlDialog.getDialogPane().setPrefSize(
                primaryStage.getWidth() * .75, primaryStage.getHeight() * .75);
        sqlDialog.getDialogPane().setContent(sqlTextArea);
        sqlDialog.setTitle("Generated SQL");

        Button copyButton = (Button) sqlDialog.getDialogPane().lookupButton(copyButtonType);
        copyButton.addEventFilter(ActionEvent.ACTION, event -> {
            sqlTextArea.requestFocus();
            sqlTextArea.selectAll();

            Clipboard clipboard = Clipboard.getSystemClipboard();
            ClipboardContent clipboardContent = new ClipboardContent();
            clipboardContent.putString(sqlTextArea.getText());
            clipboard.setContent(clipboardContent);

            event.consume();
        });

        sqlDialog.showAndWait();
    }

    private void showValidationErrorsDialog() {
        Alert errorAlert = new Alert(Alert.AlertType.NONE, null, ButtonType.OK);
        errorAlert.setTitle("Cannot Generate SQL");
        errorAlert.setHeaderText(
                "There were errors in the database.\nClick an error to show reference in diagram.");
        ScrollPane errorsPane = new ScrollPane();
        ObservableList<ValidationError> errors =
                FXCollections.observableArrayList(activeDiagram.getValidator().getErrors());
        ListView<ValidationError> errorsList = new ListView<>(errors);
        errorsPane.setFitToHeight(true);
        errorsPane.setFitToWidth(true);

        errorsList.setOnMouseClicked(event -> {
            ValidationError selected = errorsList.getSelectionModel().getSelectedItem();

            if (selected != null) {
                SelectionManager.getInstance().deselectAll();
                SelectionManager.getInstance().select(selected.reference);
            }
        });

        errorsList.setPrefSize(
                primaryStage.getWidth() * .5, primaryStage.getHeight() * .5);
        errorsPane.setContent(errorsList);
        errorAlert.getDialogPane().setContent(errorsPane);
        errorAlert.initModality(Modality.NONE);
        errorAlert.setResizable(true);
        errorAlert.show();
    }

    private void showAboutWindow() {
        Alert aboutWindow = new Alert(Alert.AlertType.INFORMATION, null, ButtonType.CLOSE);
        aboutWindow.setTitle("About Visual Database Generator");
        aboutWindow.setHeaderText("Visual Database Generator");
        aboutWindow.setContentText(aboutText);
        aboutWindow.getDialogPane().setPrefWidth(400);
        aboutWindow.showAndWait();
    }

    private String loadAboutTextFromFile() {
        String contents = "";

        try {
            contents = new String(Files.readAllBytes(Paths.get("code-preamble.txt")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return contents;
    }

    public void newDiagram() {
        x = 10;
        y = 10;

        closeDialogWindows();

        if (activeDiagram != null) {
            promptToSave();
        }

        TextInputDialog nameDialog = new TextInputDialog("Default_Name");
        nameDialog.setTitle("New Diagram");
        nameDialog.setHeaderText("This database requires a name.");
        nameDialog.setContentText("Enter a valid name for the database:");
        nameDialog.getEditor().setBorder(new Border(new BorderStroke(Paint.valueOf(Color.TRANSPARENT.toString()),
                                                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        final Button okBtn = (Button) nameDialog.getDialogPane().lookupButton(ButtonType.OK);
        final TextField nameField = nameDialog.getEditor();
        nameField.textProperty().addListener((observable, oldValue, newValue) -> {
            okBtn.setDisable(newValue.endsWith(" ")||newValue.length() < 1
                            ||newValue.length() > 64);
            if(okBtn.isDisabled()) {
                nameField.setTooltip(new Tooltip("Database name must:\n" +
                                                        "-be between 1 and 64 characters\n-not end with a space"));
                nameField.setBorder(new Border(new BorderStroke(Paint.valueOf(Color.RED.toString()),
                                                 BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));


            } else {
                nameField.setBorder(new Border(
                        new BorderStroke(Paint.valueOf(Color.TRANSPARENT.toString()),
                        BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
                nameField.setTooltip(null);
            }
        });

        Optional<String> name = nameDialog.showAndWait();

        if (name.isPresent()) {
            primaryStage.setTitle(name.get() + " - Visual Database Generator");

            activeDiagram = new Diagram(name.get());

            diagramArea.getChildren().add(activeDiagram);
            diagramScrollPane.setFitToWidth(false);
            diagramScrollPane.setFitToHeight(false);

            // Enable toolbar items
            addEntityBtn.setDisable(false);
            addRelationshipBtn.setDisable(false);
        }

    }

    public void closeActiveDiagram() {
        closeDialogWindows();

        activeDiagram = null;
        diagramArea.getChildren().clear();
        diagramScrollPane.setFitToWidth(true);
        diagramScrollPane.setFitToHeight(true);

        primaryStage.setTitle("Visual Database Generator");

        // Disable toolbar items
        for (Node item : toolbar.getChildren()) {
            item.setDisable(true);
        }

        SelectionManager.getInstance().deselectAll();
    }

    public void saveActiveDiagram() {
        if (activeDiagram != null) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save Diagram File");
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("Diagram Files", "*.vdb"));

            if (activeDiagram.getSaveFile() != null) {
                fileChooser.setInitialDirectory(activeDiagram.getSaveFile().getParentFile());
                fileChooser.setInitialFileName(activeDiagram.getSaveFile().getName());
            }

            File selectedFile = fileChooser.showSaveDialog(primaryStage);

            if (selectedFile != null) {
                try {
                    // Open output file and object stream
                    FileOutputStream fileOut = new FileOutputStream(selectedFile);
                    ObjectOutputStream outStream = new ObjectOutputStream(fileOut);

                    activeDiagram.setSaveFile(selectedFile);
                    recentSaveLoadDirectory = selectedFile.getParentFile();

                    // Stream diagram to save file
                    outStream.writeObject(activeDiagram);

                    // Clean up
                    outStream.close();
                    fileOut.close();

                    // Set primary window's name to reflect saved file
                    primaryStage.setTitle(activeDiagram.getName() + " - [" + selectedFile.getAbsolutePath()
                                            + "] - Visual Database Generator");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void promptToSave() {
        if (activeDiagram != null) {
            Alert saveAlert = new Alert(Alert.AlertType.CONFIRMATION, null,
                    ButtonType.YES, ButtonType.NO);
            saveAlert.setHeaderText("Save the open diagram?");
            saveAlert.showAndWait().ifPresent(response -> {
                if (response == ButtonType.YES) {
                    saveActiveDiagram();
                }
            });
        }
    }

    public void openDiagram() {
        closeDialogWindows();
        promptToSave();

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Diagram File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Diagram Files", "*.vdb"));

        if (recentSaveLoadDirectory != null) {
            fileChooser.setInitialDirectory(recentSaveLoadDirectory);
        }

        File selectedFile = fileChooser.showOpenDialog(primaryStage);

        if (selectedFile != null) {
            try {
                // Open input file and object stream
                FileInputStream fileIn = new FileInputStream(selectedFile);
                ObjectInputStream inStream = new ObjectInputStream(fileIn);

                // Create diagram object from save file
                Diagram openedDiagram = (Diagram) inStream.readObject();

                closeActiveDiagram();
                activeDiagram = openedDiagram;

                primaryStage.setTitle(openedDiagram.getName() + " - [" + openedDiagram.getSaveFile().getAbsolutePath()
                                        + "] - Visual Database Generator");

                // Enable toolbar items
                addEntityBtn.setDisable(false);
                addRelationshipBtn.setDisable(false);

                // Add opened diagram to window
                if (!diagramArea.getChildren().contains(activeDiagram)) {
                    diagramArea.getChildren().add(activeDiagram);
                }

                diagramScrollPane.setFitToWidth(false);
                diagramScrollPane.setFitToHeight(false);

                activeDiagram.setSaveFile(selectedFile);
                recentSaveLoadDirectory = selectedFile.getParentFile();

                // Clean up
                inStream.close();
                fileIn.close();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void exportDiagramImage(Diagram diagram) {
        SnapshotParameters snapshotParameters = new SnapshotParameters();
        snapshotParameters.setViewport(croppedDiagramViewport(diagram));

        WritableImage image = diagram.snapshot(snapshotParameters, null);

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export Diagram Image");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PNG Files", "*.png"));

        if (activeDiagram.getSaveFile() != null) {
            fileChooser.setInitialDirectory(activeDiagram.getSaveFile().getParentFile());
            fileChooser.setInitialFileName(activeDiagram.getSaveFile().getName());
        } else {
            fileChooser.setInitialFileName(activeDiagram.getName());
        }

        File selectedFile = fileChooser.showSaveDialog(primaryStage);

        if (selectedFile != null) {
            try {
                ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", selectedFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private Rectangle2D croppedDiagramViewport(Diagram diagram) {
        double xMargin = 10;
        double yMargin = 10;
        double minX = diagram.getWidth() - 1;
        double minY = diagram.getHeight() - 1;
        double maxX = 0;
        double maxY = 0;
        Rectangle2D viewport = new Rectangle2D(0, 0, diagram.getWidth(), diagram.getHeight());

        if (diagram.getEntities().size() > 0) {
            for (Entity entity : diagram.getEntities()) {
                Bounds bounds = entity.getBoundsInParent();

                if (bounds.getMinX() < minX)
                    minX = bounds.getMinX();

                if (bounds.getMaxX() > maxX)
                    maxX = bounds.getMaxX();

                if (bounds.getMinY() < minY)
                    minY = bounds.getMinY();

                if (bounds.getMaxY() > maxY)
                    maxY = bounds.getMaxY();
            }

            viewport = new Rectangle2D(minX - xMargin, minY - yMargin,
                    maxX - minX + 2 * xMargin, maxY - minY + 2 * yMargin);
        }

        return viewport;
    }
}
