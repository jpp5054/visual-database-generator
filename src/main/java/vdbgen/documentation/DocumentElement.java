/*
    Visual Database Generator - Database Modeling and Generation
    Copyright (C) 2017  Joseph Pomerleau, Joshua Cole,
                        Dominic Sero-Asturi, Nicholas Teslovich
    See /src/main/java/vdbgen/Main.java for full notice.
*/

package vdbgen.documentation;

import javafx.geometry.Point2D;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import java.io.IOException;

public abstract class DocumentElement {

    protected Point2D stringLocation;
    protected String name;
    protected PDType1Font font;
    protected int fontSize;
    protected int pageIndex;

    protected DocumentElement() { this(0,0); }

    protected DocumentElement(float x, float y) { stringLocation = new Point2D(x,y); }

    public void setStringLocation(float x, float y) { stringLocation = new Point2D(x,y + getFontHeight()); }

    public void setPageIndex(int page) { pageIndex = page; }

    public String getText() { return name; }

    public Point2D getStringLocation() { return stringLocation; }

    public PDType1Font getFont() { return font; }

    public int getFontSize() { return fontSize; }

    public float getFontHeight() {
        return font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;
    }

    public float getStringWidth(String s, int fs) {
        float width = 0;

        try {
            width = font.getStringWidth(s)/1000*fs;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return width;
    }

    public float getStringWidth(String s) {
       return this.getStringWidth(s, fontSize);
    }

    public int getPageIndex() {
        return pageIndex;
    }
}
