/*
    Visual Database Generator - Database Modeling and Generation
    Copyright (C) 2017  Joseph Pomerleau, Joshua Cole,
                        Dominic Sero-Asturi, Nicholas Teslovich
    See /src/main/java/vdbgen/Main.java for full notice.
*/

package vdbgen.documentation;

import org.apache.pdfbox.pdmodel.font.PDType1Font;

public class AttributeElement extends DocumentElement {

    private String dataType;
    private String keyDesignation = "";

    public AttributeElement(float x, float y, String id, String type) {
        super(x, y);
        name = id;
        dataType = type;
        font = PDType1Font.HELVETICA_BOLD;
        fontSize = 12;
    }
    public AttributeElement(String id, String type) { this(0,0, id, type); }

    public void setKeyDesignation(String s) { keyDesignation = s; }

    public String getDataType() { return dataType; }

    public String getKeyDesignation() { return keyDesignation; }
}
