/*
    Visual Database Generator - Database Modeling and Generation
    Copyright (C) 2017  Joseph Pomerleau, Joshua Cole,
                        Dominic Sero-Asturi, Nicholas Teslovich
    See /src/main/java/vdbgen/Main.java for full notice.
*/

package vdbgen.documentation;

import javafx.geometry.Point2D;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.interactive.action.PDActionGoTo;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationLink;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDBorderStyleDictionary;

public class RelationshipElement extends DocumentElement {

    private static final float ANNOTATION_Y_OFFSET = -5;

    private String relationshipType;
    private String relatedEntity;
    private Point2D annotationLocation;
    private PDAnnotationLink annotationLink;

    public RelationshipElement(float x, float y, String type, String entity) {
        super(x,y);
        relationshipType = type;
        relatedEntity = entity;
        font = PDType1Font.HELVETICA;
        fontSize = 14;
    }

    public RelationshipElement(String type, String entity) { this(0,0, type, entity); }

    public void setRelationshipType(String s) { relationshipType = s; }

    public void setRelatedEntity(String s) { relatedEntity = s; }

    public void setAnnotationLocation(float x, float y) {
        annotationLocation = new Point2D(x,y + getFontHeight() + ANNOTATION_Y_OFFSET);
    }

    public void setAnnotationLink(PDRectangle r, PDActionGoTo action, PDPage page) {
        if (annotationLink == null) {
            annotationLink = new PDAnnotationLink();
        }

        PDBorderStyleDictionary style = new PDBorderStyleDictionary();
        style.setStyle(PDBorderStyleDictionary.STYLE_UNDERLINE);
        style.setWidth(1);

        annotationLink.setPage(page);
        annotationLink.setBorderStyle(style);
        annotationLink.setRectangle(r);
        annotationLink.setAction(action);
    }

    public String getRelationshipType() { return relationshipType; }

    public String getRelatedEntity() { return relatedEntity; }

    public Point2D getAnnotationLocation() { return annotationLocation; }

    public PDAnnotationLink getAnnotationLink() { return annotationLink; }
}
