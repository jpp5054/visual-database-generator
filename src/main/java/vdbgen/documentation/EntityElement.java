/*
    Visual Database Generator - Database Modeling and Generation
    Copyright (C) 2017  Joseph Pomerleau, Joshua Cole,
                        Dominic Sero-Asturi, Nicholas Teslovich
    See /src/main/java/vdbgen/Main.java for full notice.
*/

package vdbgen.documentation;

import org.apache.pdfbox.pdmodel.font.PDType1Font;

import java.util.ArrayList;

public class EntityElement extends DocumentElement {

    private ArrayList<AttributeElement> attributeElements = new ArrayList<>();
    private ArrayList<RelationshipElement> relationshipElements = new ArrayList<>();

    public EntityElement(float x, float y, String s) {
        super(x, y);

        font = PDType1Font.HELVETICA_BOLD_OBLIQUE;
        fontSize = 20;
        name = s;
    }

    public EntityElement(String s) {
        super();

        font = PDType1Font.HELVETICA_BOLD_OBLIQUE;
        fontSize = 20;
        name = s;
    }

    public EntityElement(){
        this("");
    }

    public void addAttributeElement(AttributeElement e) { attributeElements.add(e); }

    public void addRelationshipElement(RelationshipElement e) { relationshipElements.add(e); }

    public ArrayList<AttributeElement> getAttributeElements() { return attributeElements; }

    public ArrayList<RelationshipElement> getRelationshipElements() { return relationshipElements; }
}
