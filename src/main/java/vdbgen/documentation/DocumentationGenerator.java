/*
    Visual Database Generator - Database Modeling and Generation
    Copyright (C) 2017  Joseph Pomerleau, Joshua Cole,
                        Dominic Sero-Asturi, Nicholas Teslovich
    See /src/main/java/vdbgen/Main.java for full notice.
*/

package vdbgen.documentation;

import javafx.stage.FileChooser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.interactive.action.PDActionGoTo;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDPageFitWidthDestination;
import vdbgen.controllers.ApplicationController;
import vdbgen.diagram.Attribute;
import vdbgen.diagram.Diagram;
import vdbgen.diagram.Entity;
import vdbgen.diagram.Relationship;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DocumentationGenerator {

    private static DocumentationGenerator documentationGenerator;
    private ApplicationController appController = ApplicationController.getInstance();
    private PDDocument erdDocument;
    private PDPage page;
    private PDPageContentStream contentStream;
    private PDType1Font font;
    private ArrayList<EntityElement> entityElements = new ArrayList<>();
    private int pageCount = 0;
    private int fontSize;
    private float centerX, centerY, currentX, currentY, stringWidth, fontHeight;
    private float margin = 25;
    private String text;

    public void generatePDF() {
        if (appController.getActiveDiagram() != null) {
            Diagram diagram = appController.getActiveDiagram();
            ArrayList<Entity> entities = diagram.getEntities();

            erdDocument = new PDDocument();
            pageCount = 0;
            setCoverPage();

            for (Entity e : entities) {
                EntityElement entityElement = new EntityElement(e.getName());
                writeElement(entityElement);

                for (Relationship r : e.getRelationships()) {
                    RelationshipElement relationshipElement;

                    if (r.getEnd1Entity() == e) {
                        relationshipElement = new RelationshipElement(r.getTypeName(), r.getEnd2Entity().getName());
                    } else {
                        relationshipElement = new RelationshipElement(r.getTypeName(), r.getEnd1Entity().getName());
                    }

                    writeElement(relationshipElement);
                    entityElement.addRelationshipElement(relationshipElement);
                }

                for (Attribute a : e.getAttributes()) {
                    AttributeElement attributeElement = new AttributeElement(a.getName(), a.getTypeName());

                    if (a.isPrimaryKey() && a.isForeignKey()) {
                        attributeElement.setKeyDesignation("***Primary Key and Foreign Key***");
                    } else if (a.isPrimaryKey()) {
                        attributeElement.setKeyDesignation("***Primary Key***");
                    } else if (a.isForeignKey()) {
                        attributeElement.setKeyDesignation("***Foreign Key***");
                    }

                    writeElement(attributeElement);
                    entityElement.addAttributeElement(attributeElement);
                }

                currentY -= 10;
                entityElements.add(entityElement);
            }

            //add annotation links
            boolean found;
            List<PDAnnotation> annotations;

            for (EntityElement e : entityElements) {
                for (RelationshipElement r : e.getRelationshipElements()) {
                    page = erdDocument.getPage(r.getPageIndex());

                    try {
                        annotations = page.getAnnotations();

                        font = r.getFont();
                        fontSize = r.getFontSize();
                        fontHeight = r.getFontHeight();
                        stringWidth = r.getStringWidth(r.getRelatedEntity());

                        PDRectangle clickBox = new PDRectangle();
                        clickBox.setLowerLeftX((float)(r.getAnnotationLocation().getX()));
                        clickBox.setUpperRightX(clickBox.getLowerLeftX()+stringWidth);
                        clickBox.setLowerLeftY((float)(r.getAnnotationLocation().getY()-fontHeight));
                        clickBox.setUpperRightY((float)(r.getAnnotationLocation().getY()));

                        found = false;

                        for (int i = 0; !found && i < entityElements.size(); i++) {
                            if (entityElements.get(i).getText().equals(r.getRelatedEntity())) {
                                found = true;

                                PDPageFitWidthDestination destination = new PDPageFitWidthDestination();
                                destination.setFitBoundingBox(true);
                                destination.setTop((int)entityElements.get(i).getStringLocation().getY());
                                destination.setPage(erdDocument.getPage(entityElements.get(i).getPageIndex()));

                                PDActionGoTo action = new PDActionGoTo();
                                action.setDestination(destination);

                                r.setAnnotationLink(clickBox, action, page);
                                annotations.add(r.getAnnotationLink());
                            }
                        }

                        page.setAnnotations(annotations);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }

            try {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Save PDF File");
                fileChooser.getExtensionFilters().addAll(
                        new FileChooser.ExtensionFilter("PDF Files", "*.pdf"));

                if (appController.getActiveDiagram().getSaveFile() != null) {
                    fileChooser.setInitialDirectory(appController.getActiveDiagram().getSaveFile().getParentFile());

                    String initialName = appController.getActiveDiagram().getSaveFile().getName();
                    initialName = initialName.substring(0, initialName.lastIndexOf('.'));
                    fileChooser.setInitialFileName(initialName);
                }

                File selectedFile = fileChooser.showSaveDialog(appController.getPrimaryStage());

                if (selectedFile != null) {
                    erdDocument.save(selectedFile);
                }

                erdDocument.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Can't generate documentation for a diagram that isn't open...");
        }
    }

    private void setCoverPage() {
        try {
            page = newPage();

            font = PDType1Font.COURIER_BOLD_OBLIQUE;
            fontSize = 32;
            text = appController.getActiveDiagram().getName();
            centerX = page.getTrimBox().getWidth()/2;
            centerY = page.getTrimBox().getHeight()/2;
            fontHeight = font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;
            stringWidth = font.getStringWidth(text)/1000*fontSize;
            while (stringWidth > page.getTrimBox().getWidth() - margin * 2) {
                fontSize -= 2;
                stringWidth = font.getStringWidth(text)/1000*fontSize;
            }

            contentStream = new PDPageContentStream(erdDocument, page);
            contentStream.setFont(font, fontSize);
            contentStream.beginText();
            contentStream.newLineAtOffset(centerX - stringWidth/2, centerY - fontHeight/2);
            contentStream.showText(text);
            contentStream.endText();
            contentStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        page = newPage();
    }

    private void writeElement(DocumentElement element) {
        font = element.getFont();
        fontSize = element.getFontSize();
        fontHeight = element.getFontHeight();

        if (currentY - fontHeight < page.getTrimBox().getLowerLeftY()) {
            // content won't fit on page, so add a new one
            page = newPage();
        } else if (element instanceof AttributeElement) {
            if (currentY - 3 * fontHeight < page.getTrimBox().getLowerLeftY() + margin) {
                //attributes can take 3 lines unlike other elements
                //so if there aren't 3 lines available, new page
                page = newPage();
            }
        }

        try {
            element.setPageIndex(pageCount-1);
            contentStream = new PDPageContentStream(erdDocument, page,
                                                    PDPageContentStream.AppendMode.APPEND, false);
            contentStream.setFont(font, fontSize);
            contentStream.beginText();

            if (element instanceof EntityElement) {
                element.setStringLocation(currentX, currentY);
                contentStream.newLineAtOffset(currentX, currentY);
                contentStream.showText(element.getText());
                currentY -= fontHeight;
            } else if (element instanceof AttributeElement) {
                AttributeElement attributeElement = (AttributeElement) element;

                attributeElement.setStringLocation(currentX + margin, currentY);
                contentStream.newLineAtOffset(currentX + margin, currentY);
                contentStream.showText("Column Name: " + attributeElement.getText());
                currentY -= fontHeight;
                contentStream.newLineAtOffset(margin, -fontHeight);
                contentStream.showText("Type: " + attributeElement.getDataType());
                currentY -= fontHeight;

                if (!attributeElement.getKeyDesignation().equals("")) {
                    contentStream.newLineAtOffset(0, -fontHeight);
                    contentStream.showText(attributeElement.getKeyDesignation());
                    currentY -= fontHeight;
                }
            } else if (element instanceof RelationshipElement) {
                RelationshipElement relationshipElement = (RelationshipElement) element;

                relationshipElement.setStringLocation(currentX + margin/2, currentY);
                contentStream.newLineAtOffset(currentX + margin/2, currentY);
                text = "is in a " + relationshipElement.getRelationshipType() + " relationship with ";
                stringWidth = font.getStringWidth(text) / 1000 * fontSize;

                relationshipElement.setAnnotationLocation(currentX + margin/2 + stringWidth, currentY);
                text += relationshipElement.getRelatedEntity();

                contentStream.showText(text);
                currentY -= fontHeight;
            }

            contentStream.endText();
            contentStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private PDPage newPage() {
        PDPage page = new PDPage();

        page.setTrimBox(PDRectangle.LETTER);
        erdDocument.addPage(page);
        pageCount++;
        currentX = page.getTrimBox().getLowerLeftX() + 25;
        currentY = page.getTrimBox().getUpperRightY() - 50;

        return page;
    }

    public static DocumentationGenerator getInstance() {
        if (documentationGenerator == null) {
            documentationGenerator = new DocumentationGenerator();
        }

        return documentationGenerator;
    }
}
