# Visual Database Generator

## [Project website](https://sites.google.com/view/vdbgen)

## Contributors:
Joseph Pomerleau, Nicholas Teslovich, Joshua Cole, and Dominic Sero-Asturi

---

## Building from Source:
This project uses features of **JDK 8** (including JavaFX 8) so it will require that version to be used when building.

This project has the following dependencies:

- [PDFBox](https://pdfbox.apache.org/2.0/getting-started.html)

They will need to be downloaded and added to the project's class path before the project will successfully build.

**In the future, this project may be converted to a [Maven](https://maven.apache.org/) project for better dependency handling.**